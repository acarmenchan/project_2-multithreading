import java.util.ArrayList;

public class CinemaSimulation {

	public static int NUM_CUSTOMERS = 50;
	
	private BoxOfficeAgent boxOfficeAgent0;
	private BoxOfficeAgent boxOfficeAgent1;
	private TicketTaker ticketTaker;
	private ConcessionStandWorker concessionStandWorker;
	
//	private ArrayList<Customer> customers;
	
	CinemaSimulation(ArrayList<Theater> theaterList) {
		System.out.println("Theater is open");
		
		// CREATE ALL THE CUSTOMERS WITH RANDOM MOVIES
//		this.customers = new ArrayList<Customer>(NUM_CUSTOMERS);
//		for (int i = 0; i < NUM_CUSTOMERS; i++) {
//			int randomIndex = Utilities.getRandomMovieNumber(theaterList);
//			String movieTitle = theaterList.get(randomIndex).getMovieTitle();
//			this.customers.add(new Customer(i, movieTitle));
//		}
		
		this.boxOfficeAgent0 = new BoxOfficeAgent(0, theaterList);
		this.boxOfficeAgent1 = new BoxOfficeAgent(1, theaterList);
	}
	
	void runSimulation() {
//		for( int i = 0; i < NUM_CUSTOMERS; i++ ) {
//			Thread custThread = new Thread(this.customers.get(i));
//			custThread.start();
//		}
		
		Thread agentThread0 = new Thread(this.boxOfficeAgent0);
		agentThread0.start();
		
		Thread agentThread1 = new Thread(this.boxOfficeAgent1);
		agentThread1.start();

		
		Thread ticketThread = new Thread(this.ticketTaker);
		ticketThread.start();
		
		Thread concessionThread = new Thread(this.concessionStandWorker);
		concessionThread.start();
	}
}
