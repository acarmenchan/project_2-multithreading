import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class BoxOfficeAgent implements Runnable {

	private int agentNumber;
	private ArrayList<Theater> theaterList;
	private static Semaphore nowServingMutex = new Semaphore(1);
	private static int nowServing = -1;
	
	public BoxOfficeAgent(int number, ArrayList<Theater> theaterList) {
		this.agentNumber = number; 
		this.theaterList = theaterList;
		
		System.out.println("Box office agent " + this.agentNumber + " created");
	}

	@Override
	public void run() {
		try {
			boolean didServeACustomer;
			do {
				didServeACustomer = this.serveNextCustomer();
				
				Thread.sleep(100);
				
				Semaphores.box_office.release();
			} while (didServeACustomer);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean serveNextCustomer() throws InterruptedException {
		
		incrementNowServing();
		if ( BoxOfficeAgent.nowServing < CinemaSimulation.NUM_CUSTOMERS ) {

			Theater theater = theaterList.get(Utilities.getRandomTheaterNumber(theaterList));
			Customer currentCustomer = createCustomer(theater);
			
			if (theater.reserveSeat()) {
				sellTicket(currentCustomer);
				
				//send customer to the ticket taker line
			}
			else {
				// theater is full, send customer home
				sendCustomerHome(currentCustomer);
			}
			return true; //we served a customer
		}
		else {
			return false; //no more customers left
		}
	}

	private void incrementNowServing() throws InterruptedException{
		BoxOfficeAgent.nowServingMutex.acquire();
		BoxOfficeAgent.nowServing++;
		BoxOfficeAgent.nowServingMutex.release();
	}
	
	//*** OUTPUT ***//
	private Customer createCustomer(Theater theater) {
		Customer customer = new Customer(BoxOfficeAgent.nowServing, theater.getMovieTitle());
		System.out.println("Customer " + customer.getNumber() + " created, buying ticket to + " + theater.getMovieTitle() );
		
		return customer;
	}
	
	private void sellTicket(Customer currentCustomer) {
		System.out.println("Box office agent " + this.agentNumber + " sold ticket for " + currentCustomer.getMovieSelected() + " to customer " + currentCustomer.getNumber());
	}
	
	private void sendCustomerHome(Customer currentCustomer) {
		System.out.println("Movie is sold out, customer " + currentCustomer.getNumber() + " leaves");
	}
}
