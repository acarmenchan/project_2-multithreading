
public class Customer implements Runnable {

	private int number;
	private String movieSelected;
	
	public Customer(int customerNumber, String movieSelected) {
		this.number = customerNumber;
		this.movieSelected = movieSelected;
		
//		System.out.println("Customer " + this.number + " created");
	}
	
	@Override
	public void run() {
		try {
//			System.out.println("For customer " + this.number + " THE NUMBER OF AVAILABLE PERMITS IS: " + Semaphores.box_office.availablePermits());
			Semaphores.box_office.acquire();
			System.out.println("Customer number " + this.number + " created, wants to watch " + this.movieSelected);
			
			Semaphores.ticket_taker.acquire();
			
			if( Utilities.getRandomConcessionVisit() ) {
				Semaphores.concession_stand.acquire();
			}
			
			System.out.println("Customer theater entering to watch movie.");

		} catch (InterruptedException e) {
//			e.printStackTrace();
		}
		
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getMovieSelected() {
		return movieSelected;
	}

	public void setMovieSelected(String movieSelected) {
		this.movieSelected = movieSelected;
	}
}
