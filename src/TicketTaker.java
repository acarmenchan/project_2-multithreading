
public class TicketTaker implements Runnable {

	public TicketTaker() {
		System.out.println("Ticket taker created");
	}
	
	public void takeTicket() {
		System.out.println("Ticket taken from customer ");
	}

	@Override
	public void run() {
		this.takeTicket();
		Semaphores.ticket_taker.release();
	}
}
