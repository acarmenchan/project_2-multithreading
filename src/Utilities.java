import java.util.ArrayList;
import java.util.Random;

public class Utilities {
	
	private static Random random = new Random();
	
	public static boolean isDigit(char character) {
		return (character >='0' && character <='9');
	}
	
	public static int getRandomTheaterNumber(ArrayList<Theater> theaterList) 
	{
		return random.nextInt(theaterList.size());		//0 - 4
	
//		if ( DEBUG_MODE ) { System.out.println("Generating random number... "); }
	}
	
	public static boolean getRandomConcessionVisit() {
		// 50% chance of visiting concession
		int chance = random.nextInt(2);	// returns either 0 or 1
		
		if( chance == 0 ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public enum ConcessionSnack {
		POPCORN,
		SODA,
		BOTH
	}
	
	public static int getRandomConcessionSnack() {
		
		return random.nextInt(3) + 1;
	}
}
