import java.util.concurrent.Semaphore;

public class Semaphores {
	
	// box office sems
	public static Semaphore box_office       = new Semaphore(2);		// 2 agents
	public static Semaphore ticket_taker     = new Semaphore(1);
	public static Semaphore concession_stand = new Semaphore(1);		// 2 agents
}
