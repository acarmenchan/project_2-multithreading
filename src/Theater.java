import java.util.concurrent.Semaphore;

public class Theater {
	
	private String movieTitle;
	private int maxCapacity;
	
	private Semaphore seatsAvailable; 
//	private Semaphore seatsAvailableMutex = new Semaphore(1);
//	private int seatsAvailable;
	
	Theater(String title, int seats) {
		this.movieTitle = title;
		this.maxCapacity = seats; 
		this.seatsAvailable = new Semaphore(this.maxCapacity);
//		this.seatsAvailable = this.maxCapacity;
	}
	
	public String getMovieTitle() {
		return movieTitle;
	}
	
	public int getMaxCapacity() {
		return maxCapacity;
	}

	public boolean reserveSeat() {
		if (this.seatsAvailable.availablePermits() > 0) {
			try {
				this.seatsAvailable.acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return true;
		}
		return false;
	}
}
