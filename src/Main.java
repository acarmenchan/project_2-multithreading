public class Main {

	private static boolean DEBUG_MODE = true;
	
	public static void main(String[] args) {
		
		String inputFileName = "movies_original.txt";
		
		TheaterReader theaterReader = new TheaterReader(inputFileName);		
		
		if (DEBUG_MODE) { theaterReader.DEBUG_printRawFile(); }
		if (DEBUG_MODE) { theaterReader.DEBUG_printTheaterList(); }
		
		CinemaSimulation cinemaSimulation = new CinemaSimulation(theaterReader.getTheaterList());
		cinemaSimulation.runSimulation();
		
	}
}
