import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class TheaterReader {
	
	private ArrayList<String> moviesFile = new ArrayList<String>(); 
	private ArrayList<Theater> theaterList = new ArrayList<Theater>();
	

	TheaterReader(String fileName) {
		this.moviesFile = this.readFile(fileName);
		this.theaterList = this.buildTheaters();
	}
	
	public ArrayList<Theater> getTheaterList() {
		return this.theaterList;
	}
	
	public ArrayList<String> readFile(String fileName) {
		System.out.println("===============================");
		System.out.println("   LOADING FILE: " + fileName);
		System.out.println("===============================");
		System.out.println();
		
		String lineIn;
		
		// OPEN INPUT FILE
		File inFile = new File (fileName);
		Scanner fileScan;
		
		try {
			fileScan = new Scanner(inFile);
			
			// ADD EACH LINE TO ARRAY LIST
			while (fileScan.hasNextLine()) {
				lineIn = fileScan.nextLine();
				this.moviesFile.add(lineIn);
			}
			
			// CLOSE INPUT FILE
			fileScan.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Could not find file named: " + fileName);
		}
		
		return this.moviesFile;
	}
	
	public void DEBUG_printRawFile() {
		System.out.println("======================================");
		System.out.println("<DEBUG> PRINTING RAW FILE DATA <DEBUG>");
		System.out.println("======================================");
		
		int lineNumber = 1;
		for (String line : this.moviesFile) {
			System.out.println(lineNumber + " : " + line);
			lineNumber++;
		}
	
		System.out.println();
	}
	
	public ArrayList<Theater> buildTheaters() {
		
		for (String line : this.moviesFile) {
			String movieTitle = "";
			String movieSeats = "";
			int titleEndsHere = 0;
			
			for (int i = (line.length() - 1); i > 0; i--) {
				if (Utilities.isDigit(line.charAt(i))) {
					movieSeats += line.charAt(i);
				}
				
				else {
					titleEndsHere = i;
					break;
				}
			}
			
			movieSeats = new StringBuilder(movieSeats).reverse().toString();
			int seats = Integer.parseInt(movieSeats.trim());
			
			for (int x = 0; x < titleEndsHere; x++) {
				movieTitle += line.charAt(x);
			}
			
			this.theaterList.add(new Theater(movieTitle, seats));
		}
		return this.theaterList;
	}
	
	public void DEBUG_printTheaterList() {
		System.out.println("===============================");
		System.out.println("<DEBUG> THEATER OBJECTS <DEBUG>");
		System.out.println("===============================");
		
		int lineNumber = 1;
		for (Theater theater : this.theaterList) {
			System.out.println("Theater " + lineNumber + " showing " + theater.getMovieTitle() + " with " + theater.getMaxCapacity() + " seats available");
			lineNumber++;
		}
		
		System.out.println();
	}	
}
