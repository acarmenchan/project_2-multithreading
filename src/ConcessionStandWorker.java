import java.util.ArrayList;

public class ConcessionStandWorker implements Runnable {
	
	String snack;
	private ArrayList<Customer> customers;
	
	public ConcessionStandWorker(ArrayList<Customer> customers) {
		System.out.println("Concession stand worker created");
	}
	
	public void takeOrder() {
		System.out.println("Order for " + this.snack + " taken from customer " );
	}
	
	public void giveOrder() {
		System.out.println(this.snack + " given to customer ");
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	}
}
